// load ENV file
require('dotenv').config();

const nodeFetch = require('node-fetch');
const Telegram = require('telegraf/telegram');

const CHECKING_INTERVAL = 1000 * 60; // 1 min
const REQUEST_TIMEOUT = 15000; // 15 sec

const telegram = new Telegram(process.env.TELEGRAM_BOT_TOKEN);
const domains = process.env.DOMAIN_LIST.split(' ');
const adminId = process.env.ADMIN_ID;

const checkDomain = async (domain) => {
  let timeout = null;
  const data = await Promise.race([
    nodeFetch(domain),
    new Promise((resolve, reject) => {
      timeout = setTimeout(() => {
        reject(new Error(`request timeout ${domain}`));
      }, REQUEST_TIMEOUT);
    }),
  ]);

  clearTimeout(timeout);

  if (!data.ok) {
    throw new Error(`checking error ${domain}`);
  }
};

const checkDoamins = async () => {
  for (let i = 0; i < domains.length; i += 1) {
    const domain = domains[i];

    try {
      await checkDomain(domain);
    } catch (err) {
      console.log(err);
      await telegram.sendMessage(adminId, err.message)
        .catch((tgErr) => {
          console.log('send to tg error', tgErr);
        });
    }
  }

  console.log('checking is finished');
};


// first check
checkDoamins();
// interval
setInterval(() => {
  checkDoamins();
}, CHECKING_INTERVAL);
